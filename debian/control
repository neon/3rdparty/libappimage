Source: libappimage
Priority: optional
Maintainer: Scarlett Moore <sgmoore@kde.org>
Build-Depends: cmake,
               debhelper (>= 12),
               desktop-file-utils,
               libarchive-dev,
               libboost-filesystem-dev,
               libboost-dev,
               libcairo2-dev,
               libglib2.0-dev,
               libgtest-dev,
               liblzma-dev,
               librsvg2-dev,
               libsquashfuse-dev,
               xxd
Standards-Version: 4.3.0
Section: libs
Homepage: https://github.com/AppImage/libappimage
Vcs-Browser: https://salsa.debian.org/sgclark-guest/libappimage
Vcs-Git: https://salsa.debian.org/sgclark-guest/libappimage.git

Package: libappimage-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libappimage1.0 (= ${binary:Version}),
         libcairo2-dev,
         librsvg2-dev,
         ${misc:Depends}
Description: Development files for libappimage
 Core library of the AppImage project. Reference implementation
 of the AppImage specification. https://appimage.org
 .
 AppImage provides a way for upstream developers to provide
 “native” binaries for Linux users just the same way they could
 do for other operating systems. It allow packaging applications
 for any common Linux based operating system, e.g., Ubuntu,
 Debian, openSUSE, RHEL, CentOS, Fedora etc. AppImages
 come with all dependencies that cannot be assumed to be part
 of each target system in a recent enough version and will run
 on most Linux distributions without further modifications.
 .
 AppImage is the format, and AppImageKit provides a
 reference implementation.
 .
 This package contains the development files for libappimage.

Package: libappimage1.0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: libappimage0 (= 1.0.2-0xneon+20.04+focal+build1),
        libappimage0 (= 1.0.3-0xneon+20.04+focal+build3),
        libappimage0 (= 1.0.3-0xneon+20.04+focal+build4),
        libappimage0 (= 1.0.3-0xneon+20.04+focal+build5),
        libappimage0 (= 1.0.3-0xneon+20.04+focal+build6),
        libappimage0 (= 1.0.3-0xneon+20.04+focal+build8)
Replaces: libappimage0 (= 1.0.2-0xneon+20.04+focal+build1),
          libappimage0 (= 1.0.3-0xneon+20.04+focal+build3),
          libappimage0 (= 1.0.3-0xneon+20.04+focal+build4),
          libappimage0 (= 1.0.3-0xneon+20.04+focal+build5),
          libappimage0 (= 1.0.3-0xneon+20.04+focal+build6),
          libappimage0 (= 1.0.3-0xneon+20.04+focal+build8)
Description: Core library for appimage
 Core library of the AppImage project. Reference implementation
 of the AppImage specification. https://appimage.org
 .
 AppImage provides a way for upstream developers to provide
 “native” binaries for Linux users just the same way they could
 do for other operating systems. It allow packaging applications
 for any common Linux based operating system, e.g., Ubuntu,
 Debian, openSUSE, RHEL, CentOS, Fedora etc. AppImages
 come with all dependencies that cannot be assumed to be part
 of each target system in a recent enough version and will run
 on most Linux distributions without further modifications.
 .
 AppImage is the format, and AppImageKit provides a
 reference implementation.
 .
 This library is used by some tools used to ease the installation
 and usage of appimages.
 .
 This package contains the library for libappimage.
